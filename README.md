# Config Ignore Features

## INTRODUCTION

This modules is a tool to ignore all the configuration stored in feature modules while using config sync.

Lets say that you do would like to share a content type between your websites, but every time you import your changes, all the config is broken.

Then this module is what you are looking for.

## REQUIREMENTS

You will need the `config_ignore` and `features` module to be enabled.

## INSTALLATION

Consult https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
to see how to install and manage modules in Drupal 8.

## CONFIGURATION

There is no configuration needed!
